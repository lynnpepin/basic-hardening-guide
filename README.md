# Linux Basic Server Setup Guide

Here is a basic security guide I follow when I first set up an Ubuntu Linux server.

This guide is aimed towards someone who is comfortable with the command line, ssh and ssh public/private key pairs, and networking concepts. 

**Important disclaimers!**

 * This is not an all encompassing guide to server security! These measures are the mere basics.
 * The commands you'll use here will differ from server to server, and may be outdated in a few years.
 * This is a **work in progress** document.
 * This guide is written by some grad student! Take it with a grain of salt!

Some other relevant and related guides:
1. https://www.linode.com/docs/security/securing-your-server/
2. https://www.digitalocean.com/community/questions/best-practices-for-hardening-new-sever-in-2017
3. https://www.digitalocean.com/community/tutorials/how-to-harden-openssh-on-ubuntu-18-04

Also consider the **University of Connecticut Baseline Configuration Standard**: https://security.uconn.edu/baseline-configuration-standard-linux/


## 0. Before we start: Don't connect it to the internet right away!

> **TLDR:** Linux servers should be secured before you first open their services.

Linux servers are generally not secure by default. If you connect it to the internet with port 22 open and the SSH server running, **it could even be compromised within seconds!**

> **Example:** You install Linux, you open port 22 in your router, and you start the SSH server *before* doing any basic hardening. Root login is enabled, and the password is blank. Port-scanners find port 22 open on your IP, and blam! They have total access to your machine.


In default Ubuntu installations, SSH and other dangerous services are not enabled by default, but this is not true for every distro. So, if you are setting up a computer as a server (e.g. for a homelab), try to avoid port-forwarding SSH before finishing this guide. (Just in case!)

SSH is just one of many services that can be compromised. Open mail relays, open FTP servers, etc. are all threats to consider. This article focuses primarily on hardening SSH as it will be the primary method for accessing your server.


## 1. Set up a user account

> **TLDR:** Make a new user account for yourself, give it Sudoer privilege if you wish, and add a public key to the account. Don't forget to change passwords!

In general, you want to do as little work as the `root` user as possible. This is because `root` has access to the entire filesystem and can do anything, meaning one little mistake can cause system instability or even a simple piece of malware can become a persistent threat. You can repeat this section for multiple accounts.

When I make a new user, I usually do the following commands.

> **Protip:** These commands must be run as root, or using `sudo`. As a shortcut, you can type `sudo !!`  to rerun the previous command as sudo.

To create a new user, user

`useradd -m usernamame_here -s /bin/bash`

The `-m` flag sets a home directory, and the `-s` flag sets the default shell. Next, we modify the user to add them to the group "sudo":

`usermod -aG sudo username_here`

where `-aG`means "append to group list".

### 1.1 Adding SSH public keys to a user account

> **TLDR:** Create `~/.ssh` with permissions `700` and `~/.ssh/authorized_keys`with permissions `600`. Make sure to `chown username_here:username_here`.

Next, you want to add an ssh public key to the users account. **This section assumes you are familiar with how to generate and use .ssh key pairs.**

Let us add a `.ssh` folder and a `authorized_keys` file to the users home directory:

`mkdir /home/username_here/.ssh`
`touch /home/username_here/.ssh/authorized_keys`

The `touch` command creates a file if it does not exist, similar to `mkdir`. Next, we need to change the ownership and restrict the permissions of these folders:

`chown -R username_here:username_here /home/username_here/.ssh`
`chmod 700 /home/username_here/.ssh`
`chmod 600 /home/username_here/.ssh/authorized_keys`

Finally, append the key to `authorized_keys`. You can do so in one of three ways:

By pasting the key (`id_rsa.pub`) in the command line: 
`echo PASTE_KEY_HERE >> /home/username_here/.ssh/authorized_keys`

By downloading `id_rsa.pub` and appending it to the file:
`cat /insert/path/here/id_rsa.pub >> /home/username_here/.ssh/authorized_keys`

or by using your favorite text editor, e.g. `vim`: 
`vim /home/username_here/.ssh/authorized_keys`

### 1.2 Login and change password

> **TLDR:** Change passwords, including those of default accounts, using `passwd`.

Don't forget to change the password to this account.  ome servers or tools come pre-installed with accounts with accounts with passwords, e.g. Kali Linux's  `root:toor`, Raspberry Pi's `pi:raspberry`, or Kodi's `root:libreelec`.

To change the password *of an account you are already logged into*, simply type `passwd`. You should see something like:

    Changing password for root
    (current) UNIX password:
    Enter new UNIX password:
    Retype new UNIX password:
    passwd: password updated successfully
		
To change another users password, simply type `sudo passwd username_here`. 

### 1.3 (Optional) Customize `~/.bashrc`

The `.bashrc` file contains settings and options for the default shell, `bash`.  It is useful to write alias functions, change colors, set the path, etc.!

Here are some changes I made to my `~/.bashrc` that I found helpful.

#### 1.3.1 (Optional) Increase history length

The `history` command prints out a list of recent commands. E.g. `history 20` will show you your 20 most recent commands. However, this list is short and runs out of space quickly. To make it longer, modify two parameters in `.bashrc`. I have the following set:

    HISTSIZE=10000000
    HISTFILESIZE=20000000

#### 1.3.2 (Optional) Write useful aliases

I also make sparing use of 'aliases', which are basically shortcut commands. For example, I often search through my history using `grep`, e.g. as `history | grep some_old_command`. I'd like to shorten this to `hgrep some_old_command`. So, I add this line to my `.bashrc`:

`alias hgrep="history | grep"`

#### 1.3.3 (Optional) Add useful binaries to `$PATH`

When you run a program such as `vim`, Bash first needs to find `vim`. It does this by searching through the `$PATH` variable.

You can add other folders to the $PATH in `.bashrc`, so that Bash will search through those as well. As an example, to add `/home/some_username/` to the path, add this to your `.bashrc` file:

`export PATH="$PATH:/home/some_username/"`


## 2. Harden SSH

> **TLDR:** Modify `/etc/ssh/sshd_config` to (1) `PermitRootLogin no` (2) `PasswordAuthentication no`.

In this section, we'll cover **basic** measures to harden SSH. The most important security measure is to make sure SSH *only performs authentication using public keys*, i.e. users can not log in with only a password.

Almost everything we need to do is modify the `/etc/ssh/sshd_config` file.  You can use your favorite text editor here. For starters, it is always good practice to backup configuration files before you mess with them!

`sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak`

Next, let's modify add or uncomment the following two lines in the file. First, open the file in a text editor, e.g. `sudo vim /etc/ssh/sshd_config`.

Setting `PermitRootLogin no` will prohibit ssh login to root.

Setting `PasswordAuthentication no` will require key authentication. **Make sure you have set up ssh keys!**

Once you have set up the config file, confirm the configuration is valid:

`sudo sshd -t`

If there is no output, then the configuration is valid. You can now restart the server to activate the configuration:

`sudo systemctl restart sshd`

(alternatively: `sudo service sshd restart`)

### 2.1. (Optional) Further hardening in `sshd_config`

> **TLDR:** Configure `MaxAuthTries` and `MaxSessions` while you're editing `sshd_config`.

You can further harden SSH by modifying the following lines:

Reduce the maximum amount of login attempts before being banned:

`MaxAuthTries 5`

Reduce the maximum number of sessions:

`MaxSessions 10`

Set `X11Forwarding` to `No` if you don't expect to need X11:

`X11Forwarding no`

You can also setup an IP address allowlist by modifying `AllowUsers`. This allows only certain specified IPs to attempt to access via SSH. This is applicable only in network environments with fixed IPs, and so is generally not for remote work / work-from-home. But when applicable, it greatly reduces the attack surface. See [the DigitalOcean guide](https://www.digitalocean.com/community/tutorials/how-to-harden-openssh-on-ubuntu-18-04) for more detail. Also consider setting up a *firewall*.


### 2.2. (Optional) Consider *not* changing from port 22

> **TLDR:** I recommend you do not change the SSH port from 22.

There is some discussion about whether you should or should not assign SSH to operate on a port other than the default, 22. [This StackExchange post](https://security.stackexchange.com/questions/32308/should-i-change-the-default-ssh-port-on-linux-servers) highlights the issues nicely. Here is the top-voted answer, by security.stackexchange user 618, `scott-pack`:

> The Internet is a wild and scary place, full of malcontents whose motives range from curiosity all the way to criminal enterprise. These unsavories are constantly scanning for computers running services they hope to exploit; usually the more common services such as SSH, HTTP, FTP, etc. The scans typically fall into one of two categories:
> 
> 1. Recon scans to see what IP address have those services open.
> 2. Exploit scans against IP addresses who have been found to be running a specific service.
> 
> Considering how large the Internet is it is typically infeasible to look on every port of every IP address to find what's listening everywhere. This is the crux of the advice to change your default port. If these disaffected individuals want to find SSH servers they will start probing each IP address on port 22 (they may also add some common alternates such as 222 or 2222). Then, once they have their list of IP addresses with port 22 open, they will start their password brute force to guess usernames/passwords or launch their exploit kit of choice and start testing known (at least to them) vulnerabilities on the target system.
> 
> This means that if you change your SSH port to 34887 then that sweep will pass you on by, likely resulting in you not being targeted by the followup break-in.
> 
> Seems rosy right? There are some disadvantages though.
> 
> 1. Client Support: Everybody who connects to your server will need to know and use the changed port. If you are in a heavily managed environment, this configuration can be pushed down to the clients, or if you have few enough users it should be easy to communicate.
> 2. Documentation Exceptions: Most network devices, such as firewalls and IDSes, are pre-setup for common services to be run on common ports. Any firewall rules related to this service on this device will need to be inspected and possibly modified. Similarly, IDS signatures will be tweaked so as to only perform SSH inspection on port 22. You will need to modify every signature, every time they are updated, with your new port. (As a data point there are currently 136 VRT and ET snort signatures involving SSH).
> 3. System Protections: Modern Linuxes often ship with an kernel layer MAC and/or RBAC systems (e.g. SELinux on RedHat based or AppAmor on Debian based) and that are designed to only allow applications to do exactly what they're intended to do. That could range from accessing the `/etc/hosts` file, to writing to a specific file, or sending a packet out on the network. Depending on how this system is configured it may, by default, forbid `sshd` from binding to a non-standard port. You would need to maintain a local policy that would allow it.
> 4. Other Party Monitoring: If you have an external Information Security division, or outsource monitoring, then they will need to be made aware of the change. When performing a security assessment, or analyzing logs looking for security threats, if I see an SSH server running on a non-standard port (or an SSH server on a non-UNIX/Linux for that matter) I treat it as a potential backdoor and invoke the compromised system part of incident handling procedure. Sometimes it is resolved in 5 minutes after making a call to the administrator and being told it's legitimate, at which point I update documentation, other times it really is badness that gets taken care of. In any event, this can result in down-time for you or, at the least, a nerve racking call when you answer your phone and hear, "Hi, this is Bob from the Information Security Office. I have a few questions for you."
> 
> Before changing your port you need to take all of this into account so you know you're making the best decision. Some of those disadvantages may not apply, but some certainly will. Also consider what you're trying to protect yourself against. Often times it is simply easier to just configure your firewall to only allow access to 22 from specific hosts, as opposed to the whole Internet.

Also consider this comment on the same post, by security.stackexchange user 6813 `tom-marthenal`:

> Note that running SSH on a port over 1024 (i.e. a non-priveleged port) is actually potentially a security vulnerability. Only `root` can bind to privileged ports (<1024), so you can know that services running here are at least started by `root`. Let's say you run SSH on port 2222 and your sshd crashes for some reason. Now any local user can start their own (fake) `sshd` on port 2222 which might do bad things like stealing your password!

So, changing the default ssh port offers tenuous security benefits at best, but can introduce inconveniences and possible vulnerabilities. So, unless you have a good reason to, I recommend not to use a non-default SSH port.

If you *do* decide to use a non-default SSH port, consider using an un-used privileged port (i.e. within the range 1-1024). [Wikipedia has a list here](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers).


### 2.3. (Optional) Use fail2ban to rate-limit attackers.

Fail2Ban is a tool that monitors connections to SSH (and, optionally, other servers such as FTP) and logs and temporarily bans failed login attempts. [Linode has a good Fail2Ban tutorial here](https://www.linode.com/docs/security/using-fail2ban-to-secure-your-server-a-tutorial/).


## 3. Other hardening

Of course, there is more to consider than just SSH. This section provides some optional but recommended server hardening steps. This section is incomplete and rough.

### 3.1. Disable su and root

> **TLDR:** Disable login to root with `sudo passwd -l root` and uncomment `auth required pam_wheel.so` in `/etc/pam.d/su`.

Users can log-in as root (given its password) or as other users (given their passwords) by using the `su` command. This is a simple protection that can help prevent attacks from being succesful.

(Optional) Remove the root password: `sudo passwd -d root`

Disable login to root: `sudo passwd -l root` This will not disable sudo.

Disable su:

Uncomment the line `auth       required   pam_wheel.so` in `/etc/pam.d/su`.

### 3.2. Set up a firewall

> **TLDR:** If you only want to allow `ssh`, use `sudo ufw allow ssh; sudo ufw enable`.

A firewall is a useful layer of security to control the traffic that comes to your server, based on incoming/outgoing IP address, port, traffic type, etc.

Most Linux distros provide `iptables` for setting up firewalls. Ubuntu provides `ufw` (uncomplicated firewall) was an easier-to-use frontend to `iptables`.

There is a quick cheatsheet of `iptables` at http://cht.sh/iptables and of`ufw` at http://cht.sh/ufw and some more detailed guides for `ufw` [here](https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands) and [here](https://www.linode.com/docs/security/firewalls/configure-firewall-with-ufw/).

Here's a short example which will block everything except SSH:

`sudo ufw allow ssh` 
`sudo ufw enable`

### 3.3. Disable unneeded services

### 3.4. Consider disabling `root`

`sudo passwd -l root`

### 3.5. Install updates

#### 3.5.1. Use automatic "unattended updates"

## 4. Other

I like to install `vim`, `git`, `tmux`, `htop`, `conda` and `python3`. Consider also a backup service.

### 4.1. Learning Environments

Conda is a great tool for setting up learning environments. It's like a souped-up version of Python `pip`.